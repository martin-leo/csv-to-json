/* facultatif, mais affiche un message dans la console
qui nous confirme que le fichier master.js est bien chargé */
console.log('ok');

/* on associe l'identificateur element_input au premier élément input
rencontré dans la page.
Cela en utilisant la méthode querySelector
de l'objet document fourni par le navigateur */
var element_input = document.querySelector( 'input' );

// on associe l'identificateur element_output au premier élément avec la classe output
var element_output = document.querySelector( '.output' );

/* on veut déclencher la gestion du fichier sélectionné par l'utilisateur
lorsque celui-ci est indiqué par l'utilisateur. Pour cela, on demande à ce que,
lorsque l'élément input déclenche l'événement change, soit exécuté une fonction spécifique
dont on donne ici la référence en second paramètre */
element_input.addEventListener( 'change', gerer_nouveau_fichier );

/* on veut aussi mettre à jour si on change le type de sortie */
document.getElementById('js').addEventListener( 'change', gerer_nouveau_fichier );
document.getElementById('json').addEventListener( 'change', gerer_nouveau_fichier );

function gerer_nouveau_fichier ( evenement ) {
  /* Cette fonction va gérer tout nouveau fichier passé via notre input */

  if ( element_input.files.length == 0 ) { return; }

  var fichier = element_input.files[0];

  /* Le navigateur nous fournit un modèle d'objet nommé FileReader
  qui permet de lire des fichiers. Il s'agit d'un modèle qu'il faut instancier
  c'est à dire que l'on créé un file reader (lecteur de fichier) depuis le modèle FileReader
  Pour cela, on utilise le mot-clé réservé new */
  var lecteur = new FileReader();

  /* On indique que lorsque le lecteur de fichier a fini de télécharger notre fichier (load)
  l'on souhaite exécuter une fonction, à laquelle la méthode addEventListener va passer un objet événement.
  Ici on ne donne pas la référence de la fonction comme précédemment, mais l'on définit directement une fonction.
  On ne donne pas de nom à cette fonction, il s'agit d'une fonction dite anonyme */
  lecteur.addEventListener( 'load', function ( evenement ) {

    /* la chaîne CSV - du CSV sous la forme d'une unique chaîne de caractères -
    est contenu dans la propriété result de la propriété target de l'événement */
    var chaine_csv = evenement.target.result;

    /* on obtient via le parser une représentation de notre CSV sous forme de liste */
    var liste_brute = Papa.parse( chaine_csv ).data;

    /* On traite cette liste avec une fonction que l'on définie selon notre besoin. Cette fonction est définie plus bas. Le résultat est associé à l'identificateur objet_traite */
    var objet_traite = traiter_liste_brute( liste_brute );

    /* on utilise l'objet JavaScript JSON qui dispose d'une méthode pour transormer un objet javascript en chaîne de caractère au format JSON.
    On ajoute 'var data = ' devant, car ce que l'on désire est en réalité un fichier javascript pour After Effects. Et l'on souhaite que nos données soient accessibles via l'identificateur data */
    var json = JSON.stringify( objet_traite );

    // on règle l'attribut download avec le nom de fichier entrant où l'on a substitué « .js » à « .csv »

    element_output.setAttribute( 'download', fichier.name.replace( /(.csv|.tsv)$/, (document.getElementById('js').checked ? '.js' : '.json' ) ) );

    // on change le contenu du bloc « output » avec notre code
    element_output.innerHTML = (document.getElementById('js').checked ? 'var data = ' : '' ) + json;

    // on modifie l'attribut href du lien pour y mettre notre js/json avec le préfixe qui va bien pour le rendre téléchargeable
    element_output.setAttribute( 'href', 'data:text/' + (document.getElementById('js').checked ? 'js' : 'json' ) + ';charset=utf-8,' + encodeURIComponent(element_output.innerHTML) );
  });

  // on lance la lecture d enotre fichier avec la méthode readAsText du lecteur
  lecteur.readAsText( fichier );
}

function traiter_liste_brute ( liste_brute ) {
  /* on va traité notre CSV transormé en liste avec cette fonction, faite pour le cas spécifique où l'on a un tableur comportant des clés dans sa première colonne, des valeurs dans la seconde */

  // on créé un objet vide que l'on va remplir de couples clé/valeur
  var objet_traite = {};

  // pour chaque élément de la liste
  for (var i = 0; i < liste_brute.length; i++) {
    // on ajoute à notre objet traité la clé situé en position 0 avec la valeur située en position 1
    objet_traite[ liste_brute[i][0] ] = liste_brute[i][1];
  }

  // on retourne l'objet construit
  return objet_traite;
}
